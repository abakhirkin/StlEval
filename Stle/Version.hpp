#ifndef STLE_VERSION_HPP
#define STLE_VERSION_HPP

#include "Api.hpp"

namespace Stle {

STLE_API const char *stleVersion();

}

#endif
