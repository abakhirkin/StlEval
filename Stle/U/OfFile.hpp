#ifndef U_OF_FILE_HPP
#define U_OF_FILE_HPP

#include "Api.hpp"

namespace U {

struct U_API OfFileTag {};
constexpr OfFileTag OF_FILE;

}

#endif
