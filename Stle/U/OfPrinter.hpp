#ifndef U_OF_PRINTER_HPP
#define U_OF_PRINTER_HPP

#include "Api.hpp"

namespace U {

struct U_API OfPrinterTag {};
constexpr OfPrinterTag OF_PRINTER;

}

#endif
