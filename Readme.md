STL Evaluator
=============

[![pipeline status](https://gitlab.com/abakhirkin/StlEval/badges/master/pipeline.svg)](https://gitlab.com/abakhirkin/StlEval/commits/master)

StlEval is no longer maintained.

It was developed at Verimag, Universite Grenoble Alpes by Alexey Bakhikin,
supported by the ERC project STATOR.

- [Building From Command Line](Build.md)
- [Coding Conventions](Coding.md)
